import torch.nn as nn
import snntorch as snn

from modules.spiking_attention.module import SpikingAttention


class SpikingTransformerEncoderMLPBlock(nn.Module):
    def __init__(self, in_channels, out_channels, beta=0.9):
        super().__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.beta = beta

        self.conv = nn.Conv1d(self.in_channels, self.out_channels, kernel_size=1, padding=0, stride=1, dilation=1)
        self.bnorm = nn.BatchNorm1d(self.out_channels)
        self.lif = snn.Leaky(beta=self.beta)

    def init_mem(self):
        return self.lif.init_leaky()

    def forward(self, x, mem):
        # batch, channel, embed = x.shape
        x = self.conv(x)
        x = self.bnorm(x)
        x, mem = self.lif(x, mem)
        return x, mem


class SpikingTransformerEncoderBlock(nn.Module):
    def __init__(self, channel_size, forward_expansion=4, num_heads=8, beta=0.9):
        super().__init__()
        self.channel_size = channel_size
        self.beta = beta
        self.num_heads = num_heads
        self.attention = SpikingAttention(channel_size, num_heads=self.num_heads, beta=self.beta)

        self.mlp1 = SpikingTransformerEncoderMLPBlock(in_channels=channel_size,
                                                      out_channels=channel_size * forward_expansion)
        self.mlp2 = SpikingTransformerEncoderMLPBlock(in_channels=channel_size * forward_expansion,
                                                      out_channels=channel_size)

    def init_mem(self):
        attention_mem = self.attention.init_mem()
        mlp1_mem = self.mlp1.init_mem()
        mlp2_mem = self.mlp2.init_mem()
        return attention_mem, mlp1_mem, mlp2_mem

    def forward(self, x, mem):
        attention_mem, mlp1_mem, mlp2_mem = mem
        # batch, channel, embed = x.shape

        attention, attention_mem = self.attention(x, attention_mem)
        attention_with_shortcut = attention + x

        x, mlp1_mem = self.mlp1(attention_with_shortcut, mlp1_mem)
        x, mlp2_mem = self.mlp2(x, mlp2_mem)

        final = x + attention_with_shortcut
        return final, (attention_mem, mlp1_mem, mlp2_mem)
