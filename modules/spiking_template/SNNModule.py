from torch import nn


class SNNModule(nn.Module):
    def __init__(self, num_steps: int):
        super().__init__()
        self.num_steps = num_steps
