import torch
import torch.nn as nn
import snntorch as snn

from modules.spiking_mamba_block.module import SpikingMambaBlock
from modules.spiking_transformer_encoder_block.module import SpikingTransformerEncoderBlock


class SpikingJambaBlock(nn.Module):
    def __init__(self, channels, embedding_size, mamba_state_size, transformer_forward_expansion, transformer_num_heads,
                 num_mamba_blocks=7, transformer_location=4,
                 beta=0.9):
        super().__init__()
        self.channels = channels
        self.embedding_size = embedding_size
        self.mamba_state_size = mamba_state_size
        self.transformer_forward_expansion = transformer_forward_expansion
        self.transformer_num_heads = transformer_num_heads
        self.beta = beta
        self.num_mamba_blocks = num_mamba_blocks
        self.transformer_location = transformer_location
        self.mamba_blocks = nn.ModuleList([
            SpikingMambaBlock(channels=self.channels, embedding_size=self.embedding_size,
                              state_size=self.mamba_state_size, beta=self.beta)
            for _ in range(self.num_mamba_blocks)
        ])
        self.transformer_block = SpikingTransformerEncoderBlock(channel_size=self.channels,
                                                                forward_expansion=self.transformer_forward_expansion,
                                                                num_heads=self.transformer_num_heads, beta=self.beta)

    def init_mem(self):
        mamba_mems = [
            block.init_mem()
            for block in self.mamba_blocks
        ]
        transformer_mem = self.transformer_block.init_mem()
        return mamba_mems, transformer_mem

    def forward(self, x, mem):
        mamba_mems, transformer_mem = mem
        for i in range(0, self.transformer_location):
            x, mamba_mems[i] = self.mamba_blocks[i](x, mamba_mems[i])
        x, transformer_mem = self.transformer_block(x, transformer_mem)
        for i in range(self.transformer_location, self.num_mamba_blocks):
            x, mamba_mems[i] = self.mamba_blocks[i](x, mamba_mems[i])
        return x, (mamba_mems, transformer_mem)
