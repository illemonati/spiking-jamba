import torch
import torch.nn as nn
import snntorch as snn


class SpikingAttention(nn.Module):
    def __init__(self, channels, num_heads=8, beta=0.9):
        super().__init__()
        self.channels = channels
        self.num_heads = num_heads
        self.beta = beta

        self.lif1 = snn.Leaky(self.beta)
        self.query_conv, self.query_bnorm, self.query_lif = self.make_cbl()
        self.key_conv, self.key_bnorm, self.key_lif = self.make_cbl()
        self.value_conv, self.value_bnorm, self.value_lif = self.make_cbl()

        self.attention_conv, self.attention_bnorm, self.attention_lif = self.make_cbl(learn_beta=True)

    def make_cbl(self, learn_beta=False):
        conv = nn.Conv1d(self.channels, self.channels, kernel_size=1, padding=0, stride=1, dilation=1)
        bnorm = nn.BatchNorm1d(self.channels)
        lif = snn.Leaky(self.beta, learn_beta=learn_beta)
        return conv, bnorm, lif

    def init_mem(self):
        lif1_mem = self.lif1.init_leaky()
        query_mem = self.query_lif.init_leaky()
        key_mem = self.key_lif.init_leaky()
        value_mem = self.value_lif.init_leaky()
        attention_mem = self.attention_lif.init_leaky()
        return lif1_mem, query_mem, key_mem, value_mem, attention_mem

    def forward(self, x, mem):
        lif1_mem, query_mem, key_mem, value_mem, attention_mem = mem
        batch, channel, embed = x.shape
        x, lif1_mem = self.lif1(x, lif1_mem)

        query, query_mem = self.query_lif(self.query_bnorm(self.query_conv(x)), query_mem)
        key, key_mem = self.key_lif(self.key_bnorm(self.key_conv(x)), key_mem)
        value, value_mem = self.value_lif(self.value_bnorm(self.value_conv(x)), value_mem)

        # print(query.shape, batch, channel, embed)

        query = query.view(batch, channel, self.num_heads, embed // self.num_heads)
        key = key.view(batch, channel, self.num_heads, embed // self.num_heads)
        value = value.view(batch, channel, self.num_heads, embed // self.num_heads)

        attention = torch.einsum("bche, bche, bche -> bche", query, key, value)
        # attention = query @ key.transpose(-2, -1) @ value
        attention = attention.view(batch, channel, embed)
        attention, attention_mem = self.attention_lif(attention, attention_mem)
        attention = self.attention_bnorm(self.attention_conv(attention))

        return attention, (lif1_mem, query_mem, key_mem, value_mem, attention_mem)
