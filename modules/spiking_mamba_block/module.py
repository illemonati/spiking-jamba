import torch
import torch.nn as nn
import torch.nn.functional as F
import snntorch as snn

from modules.spiking_ssm_selection_s6.module import SpikingSSMSelectionS6


class SpikingMambaBlock(nn.Module):
    def __init__(self, channels, embedding_size, state_size, beta=0.9):
        super().__init__()
        print(channels)
        self.beta = beta
        self.channels = channels
        self.embedding_size = embedding_size
        self.state_size = state_size
        self.input_fc = nn.Linear(embedding_size, 2 * embedding_size)
        self.input_lif = snn.Leaky(self.beta)
        self.output_fc = nn.Linear(2 * embedding_size, embedding_size)
        self.output_lif = snn.Leaky(self.beta)
        self.output_fc.bias._no_weight_decay = True
        nn.init.constant_(self.output_fc.bias, 0.1)
        self.ssm_selection_s6 = SpikingSSMSelectionS6(self.embedding_size * 2, self.state_size, self.beta)
        self.conv1 = nn.Conv1d(channels, channels, kernel_size=3, padding=1)
        self.fc1 = nn.Linear(self.embedding_size * 2, self.embedding_size * 2)
        self.lif1 = snn.Leaky(self.beta)
        self.bnorm1 = nn.BatchNorm1d(channels)
        self.shortcut_fc = nn.Linear(self.embedding_size, self.embedding_size * 2)
        self.shortcut_lif = snn.Leaky(beta=beta)

    def init_mem(self):
        input_mem = self.input_lif.init_leaky()
        output_mem = self.output_lif.init_leaky()
        lif1_mem = self.lif1.init_leaky()
        ssm_mem = self.ssm_selection_s6.init_mem()
        shortcut_mem = self.shortcut_lif.init_leaky()
        return input_mem, output_mem, lif1_mem, ssm_mem, shortcut_mem

    def forward(self, x, mem):
        input_mem, output_mem, lif1_mem, ssm_mem, shortcut_mem = mem
        # print(x.shape)
        x = self.bnorm1(x)
        s, shortcut_mem = self.shortcut_lif(self.shortcut_fc(x), shortcut_mem)
        x = self.input_fc(x)
        x, input_mem = self.input_lif(x, input_mem)
        x = self.conv1(x)
        x = self.fc1(x)
        x, lif1_mem = self.lif1(x, lif1_mem)
        x, ssm_mem = self.ssm_selection_s6(x, ssm_mem)

        x += s

        x = self.output_fc(x)
        x, output_mem = self.output_lif(x, output_mem)
        return x, (input_mem, output_mem, lif1_mem, ssm_mem, shortcut_mem)
