import torch
import torch.nn as nn
import torch.nn.functional as F
import snntorch as snn


# from algo 2 of mamba paper
class SpikingSSMSelectionS6(nn.Module):
    def __init__(self, embedding_size, state_size, beta=0.9):
        super().__init__()
        self.embedding_size = embedding_size
        self.state_size = state_size
        self.beta = beta
        # x: (B, L, D)
        # A: (D, N) <- Parameter
        self.A_parameter = nn.Parameter(torch.zeros(embedding_size, state_size))
        nn.init.xavier_uniform_(self.A_parameter)
        self.A_lif = snn.Leaky(beta=self.beta)
        # B: (B, L, N) <- s_B(x)
        self.B_fc = nn.Linear(embedding_size, state_size)
        self.B_lif = snn.Leaky(beta=self.beta)
        # C: (B, L, N) <- s_C(x)
        self.C_fc = nn.Linear(embedding_size, state_size)
        self.C_lif = snn.Leaky(beta=self.beta)
        # delta: (B, L, D) <- tau_delta(Parameter + s_delta(x))
        # the tau_delta here is meant to be softmax
        # but since this is spiking we are not using it just like in transformer
        self.delta_parameter = nn.Parameter(torch.zeros(embedding_size))
        # nn.init.xavier_uniform_(self.delta_parameter)
        self.delta_fc = nn.Linear(embedding_size, embedding_size)
        self.delta_lif = snn.Leaky(beta=self.beta)

        self.D_param = nn.Parameter(torch.zeros(embedding_size))

        self.temp_start = None

        self.out_lif = snn.Leaky(beta=self.beta)

    def ssm(self, x, A_mem, B_mem, C_mem, delta_mem):
        # x: (B, L, D)
        batch, num_tokens, _ = x.shape
        # A: (D, N) <- Parameter
        A = self.A_parameter
        A, A_mem = self.A_lif(A, A_mem)
        # B: (B, L, N) <- s_B(x)
        B = self.B_fc(x)
        B, B_mem = self.B_lif(B, B_mem)
        # C: (B, L, N) <- s_C(x)
        C = self.C_fc(x)
        C, C_mem = self.C_lif(C, C_mem)
        # delta: (B, L, D) <- tau_delta(Parameter + s_delta(x))
        # but again no tau_delta ie no softmax
        delta = self.delta_parameter + self.delta_fc(x)
        # this is replaced instead by lif
        # delta = F.softmax(delta, dim=-1)
        delta, delta_mem = self.delta_lif(delta, delta_mem)
        # A_bar, B_bar: (B, L, D, N) <- discretize(delta, A, B)
        # A_bar = torch.exp(torch.einsum("bld, dn-> bldn", delta, A))
        A_bar = torch.einsum("bld, dn-> bldn", delta, A)
        B_bar = torch.einsum("bld, bln -> bldn", delta, B)
        # y: (B, L, D) <- SSM(A_bar, B_bar, C)(x)
        if self.temp_start is None:
            self.temp_start = torch.zeros((batch, self.embedding_size, self.state_size), device=A.device)
        y_stack = []
        temp = self.temp_start
        for i in range(num_tokens):
            # print(A.shape, A_bar.shape ,temp.shape)
            temp = A_bar[:, i] * temp + B_bar[:, i]
            y_partial = torch.einsum("bdn, bn -> bd", temp, C[:, i, :])
            y_stack.append(y_partial)
        y = torch.stack(y_stack, dim=1)
        y = y + x * self.D_param
        return y, (A_mem, B_mem, C_mem, delta_mem)

    def init_mem(self):
        A_mem = self.A_lif.init_leaky()
        B_mem = self.B_lif.init_leaky()
        C_mem = self.C_lif.init_leaky()
        delta_mem = self.delta_lif.init_leaky()
        out_mem = self.out_lif.init_leaky()
        return A_mem, B_mem, C_mem, delta_mem, out_mem

    def forward(self, x, mem):
        A_mem, B_mem, C_mem, delta_mem, out_mem = mem
        y, (A_mem, B_mem, C_mem, delta_mem) = self.ssm(x, A_mem, B_mem, C_mem, delta_mem)
        y, out_mem = self.out_lif(y, out_mem)
        return y, (A_mem, B_mem, C_mem, delta_mem, out_mem)
