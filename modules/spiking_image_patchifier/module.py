import torch.nn as nn
import snntorch as snn

from modules.spiking_template.SNNModule import SNNModule
from utils.general_utils import conv_to_flat, conv_size


class SpikingImagePatchifierConvBlock(nn.Module):
    def __init__(self, in_channels, out_channels, beta):
        super().__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels

        self.conv = nn.Conv2d(self.in_channels, self.out_channels, kernel_size=3, padding=0, stride=1, dilation=1,
                              bias=False)

        self.bnorm = nn.BatchNorm2d(self.out_channels)
        self.mpool = nn.MaxPool2d(kernel_size=2, stride=2, padding=1, dilation=1)
        self.lif = snn.Leaky(beta=beta)

    def init_mem(self):
        return self.lif.init_leaky()

    def forward(self, x, mem):
        x = self.conv(x)
        x = self.bnorm(x)
        x = self.mpool(x)
        x, mem = self.lif(x, mem)
        return x, mem


class SpikingImagePatchifier(nn.Module):
    def __init__(self, input_shape, out_channels=256, out_embed_size=8, num_blocks=4, beta=0.9):
        # input_shape: (C, H, W)
        super().__init__()
        self.beta = beta
        self.c, self.h, self.w = input_shape
        self.out_channels = out_channels
        self.num_blocks = num_blocks
        self.out_embed_size = out_embed_size

        self.conv_blocks = nn.ModuleList([
            SpikingImagePatchifierConvBlock(
                in_channels=self.c if i == self.num_blocks - 1 else self.out_channels // (2 ** (i + 1)),
                out_channels=self.out_channels // (2 ** i),
                beta=self.beta
            ) for i in reversed(range(0, self.num_blocks))
        ])

    def init_mem(self):
        return [
            block.init_mem()
            for block in self.conv_blocks
        ]

    def forward(self, x, mem):
        batch, channel, height, width = x.shape
        for i, block in enumerate(self.conv_blocks):
            x, mem[i] = block(x, mem[i])

        x = x.view(batch, -1, height // 2 ** self.num_blocks, width // 2 ** self.num_blocks)

        return x, mem
