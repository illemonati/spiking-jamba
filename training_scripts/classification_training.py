import os.path

import torch
import snntorch.functional as SF
from snntorch import spikegen
from torch.utils.data import DataLoader

from modules.spiking_template.SNNModule import SNNModule
from utils.general_utils import classification_metrics, project_root, make_plots


def classification_train(model: SNNModule, train_set, save_name, batch_size=128, num_epochs=10, learning_rate=5e-4,
                         device=torch.device("cuda"),
                         dtype=torch.float, print_every=50, mode="mnist", load_old_ep=None):
    torch.set_float32_matmul_precision("high")
    scaler = torch.cuda.amp.GradScaler()

    train_loader = DataLoader(train_set, batch_size=batch_size, shuffle=True, drop_last=True)
    # criterion = nn.CrossEntropyLoss()
    criterion = SF.ce_rate_loss()
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
    # scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min')
    loss_history = []
    accuracy_history = []
    save_path = f"{project_root()}/saves/{save_name}/"
    os.makedirs(save_path, exist_ok=True)
    if load_old_ep is not None:
        model.load_state_dict(torch.load(f"{save_path}/ep-{load_old_ep}.pt"))
        meta = torch.load(f"{save_path}/ep-{load_old_ep}.meta")
        loss_history = meta["loss"]
        accuracy_history = meta["accuracy"]
    model.train()
    for epoch in range(num_epochs):
        train_batch = iter(train_loader)
        for batch_step, x in enumerate(train_batch):
            if mode == 'mnist':
                data, targets = x
            elif mode == "imagenet":
                data = x["image"]
                targets = x["label"]
            data = spikegen.rate(data, model.num_steps)
            data = data.to(device)
            targets = targets.to(device)
            with torch.amp.autocast(device_type="cuda", dtype=torch.float16):
                classification_spikes = model.forward(data)
                loss = criterion(classification_spikes, targets)

            optimizer.zero_grad()
            scaler.scale(loss).backward()
            scaler.step(optimizer)
            scaler.update()


            # optimizer.step()
            scale = scaler.get_scale()
            # skip_lr_sched = (scale > scaler.get_scale())
            # if not skip_lr_sched:
            #     scheduler.step(loss)
            loss_history.append(loss.item())
            accuracy = classification_metrics(classification_spikes, targets)
            accuracy_history.append(accuracy)
            if batch_step == 0 or batch_step % print_every == print_every - 1:
                print(
                    f"Epoch: {epoch} | BatchStep: {batch_step} | Loss: {loss_history[-1]:.2f} | Acc: {accuracy:.2f}"
                )
                torch.save(model.state_dict(), f"{save_path}/ep-{epoch}.pt")
                torch.save({
                    "loss": loss_history,
                    "accuracy": accuracy_history
                }, f"{save_path}/ep-{epoch}.meta")
                make_plots(loss_history, accuracy_history, save_name=save_name)

    return loss_history, accuracy_history
