import torch
from matplotlib import pyplot as plt

from load_data.load_mnist import load_mnist
from models.spiking_transformer_image_classifier.model import SpikingTransformerImageClassifier
from training_scripts.classification_training import classification_train
from utils.general_utils import project_root, make_plots


def main():
    mnist_train, mnist_test = load_mnist()
    c, (h, w) = 1, mnist_train.data[0].shape
    print(c, h, w)
    device = torch.device("cuda")
    model = SpikingTransformerImageClassifier(input_shape=(c, h, w), oheight=7, owidth=7, oembed=64, out_features=10,
                                              num_patchifier_channels=8, num_patchifier_blocks=2, num_steps=10,
                                              num_encoder_blocks=3, beta=0.9).to(device)

    print(model)
    print(sum(p.numel() for p in model.parameters()))

    loss_hist, acc_hist = classification_train(model, mnist_train, save_name="transformer_mnist", num_epochs=15,
                                               batch_size=128, print_every=5, learning_rate=1e-4, load_old_ep=None)
    make_plots(loss_hist, acc_hist, "transformer_mnist")


if __name__ == "__main__":
    main()
