import torch
from matplotlib import pyplot as plt

from load_data.load_mnist import load_mnist
from models.basic_spiking_cnn.model import BasicSpikingCNNClassificationModel
from training_scripts.classification_training import classification_train
from utils.general_utils import project_root, make_plots


def main():
    mnist_train, mnist_test = load_mnist()
    c, (h, w) = 1, mnist_train.data[0].shape
    print(c, h, w)
    device = torch.device("cuda")
    model = BasicSpikingCNNClassificationModel((c, h, w), 10).to(device)
    loss_hist, acc_hist = classification_train(model, mnist_train, save_name="cnn_mnist", num_epochs=2)
    make_plots(loss_hist, acc_hist, "cnn_mnist")


if __name__ == "__main__":
    main()
