import torch
from matplotlib import pyplot as plt

from load_data.load_imagenet import load_imagenet
from load_data.load_mnist import load_mnist
from miscellaneous.clear_mem import clear_mem
from models.spiking_transformer_image_classifier.model import SpikingTransformerImageClassifier
from training_scripts.classification_training import classification_train
from utils.general_utils import project_root, make_plots


def main():
    clear_mem()
    imagenet_train, imagenet_test = load_imagenet()
    # print(imagenet_train[0]['image'].shape)
    c, h, w = imagenet_train[0]['image'].shape
    print(c, h, w)
    device = torch.device("cuda")
    model = SpikingTransformerImageClassifier(input_shape=(c, h, w), oheight=14, owidth=14, oembed=128,
                                              out_features=1000,
                                              num_patchifier_channels=8, num_patchifier_blocks=4, num_steps=10,
                                              num_encoder_blocks=3, beta=0.9).to(device)

    print(model)
    print(sum(p.numel() for p in model.parameters()))

    loss_hist, acc_hist = classification_train(model, imagenet_train, save_name="transformer_imagenet", num_epochs=1000,
                                               learning_rate=1e-4,
                                               batch_size=256, print_every=1, mode="imagenet")


if __name__ == "__main__":
    main()
