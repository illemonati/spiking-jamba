import torch
from matplotlib import pyplot as plt

from load_data.load_mnist import load_mnist
from models.spiking_mamba_image_classifier.model import SpikingMambaImageClassifier
from training_scripts.classification_training import classification_train
from utils.general_utils import project_root, make_plots


def main():
    mnist_train, mnist_test = load_mnist()
    c, (h, w) = 1, mnist_train.data[0].shape
    print(c, h, w)
    device = torch.device("cuda")
    model = SpikingMambaImageClassifier(input_shape=(c, h, w), oheight=7, owidth=7, oembed=64, out_features=10,
                                        num_patchifier_channels=8, num_patchifier_blocks=2, num_steps=10,
                                        mamba_state_size=32,
                                        num_encoder_blocks=5, beta=0.9).to(device)

    print(model)
    print(sum(p.numel() for p in model.parameters()))

    loss_hist, acc_hist = classification_train(model, mnist_train, save_name="mamba_mnist", num_epochs=5,
                                               batch_size=256, print_every=5, learning_rate=1e-3, load_old_ep=None)
    make_plots(loss_hist, acc_hist, "mamba_mnist")


if __name__ == "__main__":
    main()
