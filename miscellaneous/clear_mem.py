import gc
import torch


def clear_mem():
    gc.collect()
    torch.cuda.empty_cache()


if __name__ == "__main__":
    clear_mem()
