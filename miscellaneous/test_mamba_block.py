import torch
from torch.utils.data import DataLoader

from load_data.load_mnist import load_mnist
import snntorch.spikegen as spikegen

from modules.spiking_mamba_block.module import SpikingMambaBlock


def main():
    mnist_train, _ = load_mnist()
    c, (h, w) = 1, mnist_train.data[0].shape
    print(c, h, w)
    device = torch.device("cuda")
    train_loader = DataLoader(mnist_train, batch_size=128, shuffle=True, drop_last=True)
    data, targets = next(iter(train_loader))
    print(data.shape)
    data = spikegen.rate(data, 50)
    print(data.shape)
    data = data.flatten(-2, -1)
    print(data.shape)
    print(data.size(-1))
    encoder_block = SpikingMambaBlock(data.size(-2), data.size(-1), 256)
    mem = encoder_block.init_mem()
    out, mem = encoder_block(data[0], mem)
    print(out.shape)


if __name__ == "__main__":
    main()
