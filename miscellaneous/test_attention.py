import torch
from torch.utils.data import DataLoader

from load_data.load_mnist import load_mnist
import snntorch.spikegen as spikegen

from modules.spiking_attention.module import SpikingAttention


def main():
    mnist_train, _ = load_mnist()
    c, (h, w) = 1, mnist_train.data[0].shape
    print(c, h, w)
    device = torch.device("cuda")
    train_loader = DataLoader(mnist_train, batch_size=128, shuffle=True, drop_last=True)
    data, targets = next(iter(train_loader))
    print(data.shape)
    data = spikegen.rate(data, 50)
    print(data.shape)
    data = data.flatten(-2, -1)
    print(data.shape)
    s_attention = SpikingAttention(data.size(2), num_heads=8)
    s_attention.eval()
    mem = s_attention.init_mem()
    out, mem = s_attention.forward(data[0], mem)
    print(out.shape)


if __name__ == "__main__":
    main()
