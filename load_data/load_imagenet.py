from pathlib import Path

from torchvision import transforms

from utils.general_utils import project_root
from datasets import load_dataset
import datasets


def load_imagenet():
    # datasets.config.DOWNLOADED_DATASETS_PATH = Path(f"{project_root()}/data")
    # use global cache instead
    imagenet = load_dataset("imagenet-1k", trust_remote_code=True, )
    imagenet = imagenet.with_format("torch")

    transform = transforms.Compose([
        transforms.RandomChoice([transforms.Resize(256),
                                 transforms.Resize(480)]),
        transforms.RandomCrop(224),

        transforms.ToTensor(),
    ])

    normalize_transform = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])

    def apply_transform(examples):
        examples["image"] = [
            normalize_transform(transform(image)) if image.mode == "RGB" else normalize_transform(
                transforms.Lambda(lambda x: x.repeat(3, 1, 1))(
                    transform(image))) for image in examples["image"]]
        return examples

    imagenet_train = imagenet['train']
    imagenet_test = imagenet['test']
    imagenet_train.set_transform(apply_transform)
    return imagenet_train, imagenet_test


if __name__ == "__main__":
    load_imagenet()
