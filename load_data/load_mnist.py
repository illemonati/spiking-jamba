from torchvision import datasets, transforms

from utils.general_utils import project_root


def load_mnist():
    transform = transforms.Compose([
        transforms.Resize((28, 28)),
        transforms.Grayscale(),
        transforms.ToTensor(),
        transforms.Normalize((0,), (1,))])

    mnist_train = datasets.MNIST(f"{project_root()}/data/mnist", train=True, download=True, transform=transform)
    mnist_test = datasets.MNIST(f"{project_root()}/data/mnist", train=False, download=True, transform=transform)
    return mnist_train, mnist_test
