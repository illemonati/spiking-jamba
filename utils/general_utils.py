import os

import snntorch.functional as SF
import pathlib

from matplotlib import pyplot as plt


def conv_size(in_size, kernel_size=1, padding=0, stride=1, dilation=1):
    return int((in_size + 2 * padding - dilation * (kernel_size - 1) - 1) / stride + 1)


def conv_to_flat(c, h, w, in_size, kernel_size=1, padding=0, stride=1, dilation=1):
    return c * conv_size(h) * conv_size(w)


def classification_metrics(predictions, targets):
    accuracy = SF.accuracy_rate(predictions, targets)
    return accuracy


def make_plots(loss_hist, acc_hist, save_name):
    save_dir = f"{project_root()}/plots/{save_name}/"
    os.makedirs(save_dir, exist_ok=True)
    plt.title("Loss")
    plt.plot(loss_hist)
    plt.savefig(f"{save_dir}/loss.png")
    plt.clf()
    plt.title("Accuracy")
    plt.plot(acc_hist)
    plt.savefig(f"{save_dir}/acc.png")
    plt.clf()


def project_root():
    return pathlib.Path(__file__).parent.parent.resolve()
