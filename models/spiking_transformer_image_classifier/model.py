import torch
import torch.nn as nn
import snntorch as snn

from modules.spiking_image_patchifier.module import SpikingImagePatchifier
from modules.spiking_template.SNNModule import SNNModule
from modules.spiking_transformer_encoder_block.module import SpikingTransformerEncoderBlock


class SpikingTransformerImageClassifier(SNNModule):
    def __init__(self, input_shape, oheight, owidth, oembed, out_features, num_patchifier_channels=8,
                 num_patchifier_blocks=4,
                 num_encoder_blocks=3, num_steps=25, beta=0.9):
        super().__init__(num_steps)

        self.beta = beta
        self.c, self.h, self.w = input_shape
        self.patchifier = SpikingImagePatchifier(input_shape=input_shape, out_channels=num_patchifier_channels,
                                                 num_blocks=num_patchifier_blocks)
        self.fc1 = nn.Linear(oheight * owidth, oembed)
        num_embeddings = int(self.h * self.w / (self.h * self.w))
        self.pos_embed = nn.Parameter(torch.zeros(num_embeddings, oembed))
        nn.init.xavier_uniform(self.pos_embed)
        self.lif1 = snn.Leaky(beta=self.beta)
        self.encoder_blocks = nn.ModuleList([
            SpikingTransformerEncoderBlock(num_patchifier_channels)
            for _ in range(num_encoder_blocks)
        ])
        self.classification_head = nn.Linear(num_patchifier_channels * oembed, out_features)
        self.classification_lif = snn.Leaky(beta=self.beta)


    def forward(self, x):
        patchifier_mem = self.patchifier.init_mem()
        lif1_mem = self.lif1.init_leaky()
        encoder_blocks_mem = [block.init_mem() for block in self.encoder_blocks]
        classification_mem = self.classification_lif.init_leaky()

        classification_spikes = []
        # classification_mems = []

        for t in range(self.num_steps):
            xt = x[t]
            xt, patchifier_mem = self.patchifier(xt, patchifier_mem)
            xt = xt.flatten(-2, -1)
            b, c, eo = xt.shape
            xt = xt.view(b * c, eo)
            # print(b, c, eo)
            # print(xt.shape)
            xt = self.fc1(xt)
            xt = xt.view(b, c, -1)
            # print(xt.shape)
            xt += self.pos_embed
            # print(xt.shape)
            xt, lif1_mem = self.lif1(xt, lif1_mem)

            for i, block, in enumerate(self.encoder_blocks):
                xt, encoder_blocks_mem[i] = block(xt, encoder_blocks_mem[i])

            xt = xt.flatten(-2, -1)
            xt = self.classification_head(xt)
            xt, classification_mem = self.classification_lif(xt, classification_mem)
            classification_spikes.append(xt)
            # classification_mems.append(classification_mem)

        return torch.stack(classification_spikes, dim=0)
