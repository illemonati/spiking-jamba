import torch
import torch.nn as nn
import snntorch as snn

from models.spiking_template.SNNModule import SNNModel
from utils.general_utils import conv_size


class BasicSpikingCNNClassificationModel(SNNModel):
    def __init__(self, input_shape, out_features, num_steps=25, beta=0.95):
        # Input shape is (C, H, W)
        super().__init__(num_steps)

        self.beta = beta
        self.c, self.h, self.w = input_shape
        self.out_features = out_features

        self.conv1 = nn.Conv2d(
            in_channels=1,
            out_channels=32,
            kernel_size=5,
            stride=1,
            padding=2
        )
        self.bnorm1 = nn.BatchNorm2d(32)
        self.mpool1 = nn.MaxPool2d(2)
        self.lif1 = snn.Leaky(beta=self.beta)

        h1, w1 = (conv_size(x, 5, 2, 1, 1) for x in (self.h, self.w))
        h1, w1 = (x // 2 for x in (h1, w1))

        self.conv2 = nn.Conv2d(
            in_channels=32,
            out_channels=64,
            kernel_size=5,
            stride=1,
            padding=2
        )
        self.bnorm2 = nn.BatchNorm2d(64)
        self.mpool2 = nn.MaxPool2d(2)
        self.lif2 = snn.Leaky(beta=self.beta)

        h2, w2 = (conv_size(x, 5, 2, 1, 1) for x in (h1, w1))
        h2, w2 = (x // 2 for x in (h2, w2))

        self.flatten = nn.Flatten()
        self.classification_head = nn.Linear(h2 * w2 * 64, out_features)
        self.classification_lif = snn.Leaky(beta=beta)

    def forward(self, original_input):
        mem1 = self.lif1.init_leaky()
        mem2 = self.lif2.init_leaky()
        classification_mem = self.classification_lif.init_leaky()

        classification_spikes = []
        # classification_mems = []

        for t in range(self.num_steps):
            x = original_input[t]
            x = self.conv1(x)
            x = self.bnorm1(x)
            x = self.mpool1(x)
            x, mem1 = self.lif1(x, mem1)

            x = self.conv2(x)
            x = self.bnorm2(x)
            x = self.mpool2(x)
            x, mem2 = self.lif2(x, mem2)

            x = self.flatten(x)
            x = self.classification_head(x)
            x, classification_mem = self.classification_lif(x, classification_mem)
            classification_spikes.append(x)
            # classification_mems.append(classification_mem)
        return torch.stack(classification_spikes, dim=0)
